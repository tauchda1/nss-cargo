# NSS-Cargo



## Projekt Cargo
Stránka projektu Cargo

## Autoři
David Tauchen\
Denys Babin

## Non-functional requirements
1. Security:

- Ensure the protection of all user data, including login information and personal data, through SSL encryption and secure password storage.
- Implement authentication and authorization mechanisms to control access to various functions such as order placement and viewing confidential information.

2. Performance:

- Server response time should be less than 2 seconds, even during peak loads, to ensure smooth user operation.
- The system should be capable of handling a large number of simultaneous users (e.g., up to 1000 active users at any time) without degrading performance.

3. Scalability:

- Microservices should be deployable independently to facilitate scaling of specific system components as needed.
- The system architecture should support vertical scaling to manage increases in user numbers and data volume without significant changes to the system.

4. Availability:

- Achieve high availability with a target uptime of 99.5% to ensure that the system is reliable and accessible at all times.

5. Usability:

- The interface should be intuitive and easy to navigate for all types of users, supporting a responsive design that works across different browsers and screen sizes.
- Support for a multilingual interface to serve a diverse user base, potentially expanding the platform's accessibility.

6. Maintainability and Monitoring:

- Code and architecture should be designed for easy maintenance and troubleshooting, with clear documentation and adherence to coding standards.
- Implement comprehensive logging and monitoring systems to proactively detect and resolve system issues, with alerts set up for critical incidents.

## Functional requirements

1. Functionality for Guest:

- Register a new user (Register).
- Log in for an existing user (Login).

2. Functionality for Registered User:

- Log out of the system (Logout).
- Subscribe to push notifications (Subscribe to push notification).
- Unsubscribe from push notifications (Unsubscribe to push notification).

3. Functionality for Customer:

- Create a request for moving (Request moving).
- Cancel a moving request (Cancel moving).
- Leave a review after the moving is completed (Post review).

4. Functionality for Carriers:

- Change the moving status (Change moving status), including confirmation of accepting the order and updating statuses during the process.
- Sign for readiness to perform the moving (Signed for moving), i.e., confirm readiness to start the transportation.
