package Route

import (
	"RequestMicroservice/Db"
	"RequestMicroservice/Db/ent"
	"RequestMicroservice/Security"
	"context"
	"errors"
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v5"
	"os"
	"strconv"
)

const (
	OutputRequestsName = "all-requests"
	OutputRequestName  = "out-request"
)

func SetupRoutes(app *fiber.App) {
	app.Use(validateToken)
	app.Use(unpackToken)

	_ = app.Get("/request", getObfuscatedRequests)
	_ = app.Get("/request/:request_id", getRequest)

	_ = app.Post("/request", createNewRequest)

	_ = app.Patch("/request/:request_id", updateRequest)

	_ = app.Delete("/request/:request_id", deleteRequest)

	_ = app.Get("/request_lock", getUsersLockedRequests)

	_ = app.Post("/request_lock/:request_id", createNewRequestLock)

	_ = app.Delete("/request_lock/:request_id", deleteRequestLock)
}

// region Middleware
func validateToken(ctx *fiber.Ctx) error {
	token := ctx.Cookies("user-jwt", "")
	valid, token, err := Security.ValidateToken(token)
	if !valid {
		if errors.Is(err, &Security.InvalidJWTToken{}) {
			return ctx.SendStatus(fiber.StatusUnauthorized)
		} else if errors.Is(err, &Security.UnauthorizedJWTToken{}) {
			return ctx.SendStatus(fiber.StatusForbidden)
		}

		return ctx.SendStatus(480)
	}

	cookie := new(fiber.Cookie)
	cookie.Name = "user-jwt"
	cookie.Value = token

	ctx.Cookie(cookie)

	return ctx.Next()
}

func unpackToken(ctx *fiber.Ctx) error {
	token := ctx.Cookies("user-jwt", "")
	parsedToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) { return []byte(os.Getenv("JWT_SECRET")), nil })
	if err != nil {
		return ctx.SendStatus(fiber.StatusBadRequest)
	}

	claims := parsedToken.Claims.(jwt.MapClaims)

	ctx.Locals("userId", int64(claims["userId"].(float64))) // ... strictly typed languages problems
	ctx.Locals("userType", claims["userType"])

	return ctx.Next()
}

//endregion

// region Requests routes
func getObfuscatedRequests(ctx *fiber.Ctx) error {
	requests, err := Db.GetObscuredRequests()
	if err != nil {
		return ctx.SendStatus(fiber.StatusInternalServerError)
	}

	ctx.Status(fiber.StatusOK)
	output := make(map[string]any)

	output[OutputRequestsName] = requests

	return ctx.JSON(output)
}

func getRequest(ctx *fiber.Ctx) error {
	requestIdString := ctx.Params("request_id")
	requestId, err := strconv.Atoi(requestIdString)
	if err != nil {
		return ctx.SendStatus(fiber.StatusBadRequest)
	}

	request, err := Db.GetObscuredRequest(requestId)
	if err != nil {
		return ctx.SendStatus(fiber.StatusNotFound)
	}

	if request.UserID == ctx.Locals("userId").(int64) {
		request, _ = Db.GetRequest(requestId)
	}

	output := make(map[string]any)

	output[OutputRequestName] = request

	return ctx.JSON(output)
}

func createNewRequest(ctx *fiber.Ctx) error {
	request, err := Db.CreateRequest(
		ctx.Locals("userId").(int64),
		ctx.FormValue("form-send-request-description"),
		ctx.FormValue("form-send-request-town-from"),
		ctx.FormValue("form-send-request-town-to"),
		ctx.FormValue("form-send-request-address-from"),
		ctx.FormValue("form-send-request-address-to"),
	)

	if err != nil {
		return ctx.SendStatus(fiber.StatusBadRequest)
	}

	ctx.Status(fiber.StatusCreated)
	return ctx.JSON(request)
}

func updateRequest(ctx *fiber.Ctx) error {
	requestIdString := ctx.Params("request_id")
	requestId, err := strconv.Atoi(requestIdString)
	if err != nil {
		return ctx.SendStatus(fiber.StatusBadRequest)
	}

	request, err := Db.GetRequest(requestId)
	if err != nil {
		return ctx.SendStatus(fiber.StatusNotFound)
	}

	if request.UserID == ctx.Locals("userId").(int64) {
		request, err := Db.UpdateRequest(
			requestId,
			ctx.FormValue("form-send-request-description"),
			ctx.FormValue("form-send-request-town-from"),
			ctx.FormValue("form-send-request-town-to"),
			ctx.FormValue("form-send-request-address-from"),
			ctx.FormValue("form-send-request-address-to"),
		)
		if err != nil {
			return ctx.SendStatus(fiber.StatusInternalServerError)
		}

		requestLock, err := Db.GetRequestLock(requestId)
		if err == nil {
			err = Security.NotifyCarrierUpdated(requestId, ctx.Cookies("user-jwt", ""))
			if err != nil {
				return ctx.SendStatus(fiber.StatusInternalServerError)
			}

			authorId := strconv.FormatInt(request.UserID, 10)
			carrierId := strconv.FormatInt(int64(requestLock.UserID), 10)
			err = Security.NotifyCarrierCreat(ctx.Cookies("user-jwt", ""), requestIdString, authorId, carrierId, request.Description, request.TownFrom, request.TownTo, request.AddressFrom, request.AddressTo)
		}

		ctx.Status(fiber.StatusOK)
		return ctx.JSON(request)
	}

	return ctx.SendStatus(fiber.StatusUnauthorized)
}

func deleteRequest(ctx *fiber.Ctx) error {
	requestIdString := ctx.Params("request_id")
	requestId, err := strconv.Atoi(requestIdString)
	if err != nil {
		return ctx.SendStatus(fiber.StatusBadRequest)
	}

	request, err := Db.GetObscuredRequest(requestId)
	if err != nil {
		return ctx.SendStatus(fiber.StatusNotFound)
	}

	err = Security.NotifyCarrierDeleteAuthor(requestId, ctx.Cookies("user-jwt", ""))

	if request.UserID == ctx.Locals("userId").(int64) {
		err = Db.DeleteRequest(requestId)
		if err != nil {
			return ctx.SendStatus(fiber.StatusInternalServerError)
		}

		return ctx.SendStatus(fiber.StatusNoContent)
	}

	return ctx.SendStatus(fiber.StatusUnauthorized)
}

//endregion

// region Requests lock routes
func getUsersLockedRequests(ctx *fiber.Ctx) error {
	if ctx.Locals("userType") != "Carrier" {
		return ctx.SendStatus(fiber.StatusForbidden)
	}

	requestLocks, err := Db.GetUserLocks(int(ctx.Locals("userId").(int64)))
	if err != nil {
		return ctx.SendStatus(fiber.StatusInternalServerError)
	}

	output := make([]*ent.Request, 0)
	for _, iterRequestLock := range requestLocks {
		appendRequest, _ := iterRequestLock.QueryRequest().Only(context.Background())
		output = append(output, appendRequest)
	}

	return ctx.JSON(output)
}

func createNewRequestLock(ctx *fiber.Ctx) error {
	if ctx.Locals("userType") != "Carrier" {
		return ctx.SendStatus(fiber.StatusForbidden)
	}

	requestIdString := ctx.Params("request_id")
	requestId, err := strconv.Atoi(requestIdString)
	if err != nil {
		return ctx.SendStatus(fiber.StatusBadRequest)
	}

	_, err = Db.GetRequestLock(requestId)
	if err == nil {
		return ctx.SendStatus(fiber.StatusConflict)
	}

	request, err := Db.GetRequest(requestId)
	if err != nil {
		return ctx.SendStatus(fiber.StatusNotFound)
	}

	requestLock, err := Db.CreateLock(request, int(ctx.Locals("userId").(int64)))
	if err != nil {
		return ctx.SendStatus(fiber.StatusInternalServerError)
	}

	request, _ = Db.GetRequest(requestId)

	authorId := strconv.FormatInt(request.UserID, 10)
	carrierId := strconv.FormatInt(int64(requestLock.UserID), 10)

	err = Security.NotifyCarrierCreat(ctx.Cookies("user-jwt", ""), requestIdString, authorId, carrierId, request.Description, request.TownFrom, request.TownTo, request.AddressFrom, request.AddressTo)
	if err != nil {
		return ctx.SendStatus(fiber.StatusInternalServerError)
	}

	ctx.Status(fiber.StatusCreated)
	return ctx.JSON(requestLock)
}

func deleteRequestLock(ctx *fiber.Ctx) error {
	requestIdString := ctx.Params("request_id")
	requestId, err := strconv.Atoi(requestIdString)
	if err != nil {
		return ctx.SendStatus(fiber.StatusBadRequest)
	}

	requestLock, err := Db.GetRequestLock(requestId)
	if err != nil {
		return ctx.SendStatus(fiber.StatusNotFound)
	}

	if requestLock.UserID != int(ctx.Locals("userId").(int64)) {
		return ctx.SendStatus(fiber.StatusForbidden)
	}

	err = Security.NotifyCarrierDeleteCarrier(requestId, ctx.Cookies("user-jwt", ""))

	err = Db.DeleteLock(requestLock)
	if err != nil {
		return ctx.SendStatus(fiber.StatusInternalServerError)
	}

	return ctx.SendStatus(fiber.StatusNoContent)
}

//endregion
