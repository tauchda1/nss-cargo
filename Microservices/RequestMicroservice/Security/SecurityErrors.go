package Security

type InvalidJWTToken struct{}

func (err *InvalidJWTToken) Error() string {
	return "Invalid token"
}

type UnauthorizedJWTToken struct{}

func (err *UnauthorizedJWTToken) Error() string {
	return "Unauthorized token"
}
