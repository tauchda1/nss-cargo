package Security

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
)

func NotifyCarrierCreat(token string, requestId string, authorId string, carrierId string, description string, townFrom string, townTo string, addressFrom string, addressTo string) error {
	httpAgent := fiber.AcquireAgent()
	httpAgent.Cookie("user-jwt", token)

	httpArgs := fiber.AcquireArgs()
	httpArgs.Set("form-send-request-requestId", requestId)
	httpArgs.Set("form-send-request-authorId", authorId)
	httpArgs.Set("form-send-request-carrierId", carrierId)
	httpArgs.Set("form-send-request-description", description)
	httpArgs.Set("form-send-request-town-from", townFrom)
	httpArgs.Set("form-send-request-town-to", townTo)
	httpArgs.Set("form-send-request-address-from", addressFrom)
	httpArgs.Set("form-send-request-address-to", addressTo)
	httpAgent.Form(httpArgs)

	httpRequest := httpAgent.Request()
	httpRequest.Header.SetMethod(fiber.MethodPost)
	httpRequest.SetRequestURI("http://127.0.0.1:82/request")

	httpAgent.Parse()

	response, _, err := httpAgent.Bytes()
	if err != nil {
		return err[0]
	}

	if response == fiber.StatusForbidden {
		return &UnauthorizedJWTToken{}
	} else if response == fiber.StatusUnauthorized {
		return &InvalidJWTToken{}
	} else if response == fiber.StatusBadRequest {
		return fiber.ErrBadRequest
	}

	return nil
}

func NotifyCarrierUpdated(requestId int, token string) error {
	err := notifyCarrierUpdateAuthorStatus(requestId, token, "Request changed")
	if err != nil {
		return err
	}

	err = notifyCarrierUpdateCarrierStatus(requestId, token, "Request changed")
	return err
}
func notifyCarrierUpdateAuthorStatus(requestId int, token string, authorNewStatus string) error {
	httpAgent := fiber.AcquireAgent()
	httpAgent.Cookie("user-jwt", token)

	httpArgs := fiber.AcquireArgs()
	httpArgs.Set("user-jwt-token", token)
	httpArgs.Set("form-send-request-author-status", authorNewStatus)
	httpAgent.Form(httpArgs)

	httpRequest := httpAgent.Request()
	httpRequest.Header.SetMethod(fiber.MethodPatch)
	httpRequest.SetRequestURI(fmt.Sprintf("http://127.0.0.1:82/request_author/%d", requestId))

	httpAgent.Parse()

	response, _, err := httpAgent.Bytes()
	if err != nil {
		return err[0]
	}

	if response == fiber.StatusForbidden {
		return &UnauthorizedJWTToken{}
	} else if response == fiber.StatusUnauthorized {
		return &InvalidJWTToken{}
	}

	return nil
}
func notifyCarrierUpdateCarrierStatus(requestId int, token string, carrierNewStatus string) error {
	httpAgent := fiber.AcquireAgent()
	httpAgent.Cookie("user-jwt", token)

	httpArgs := fiber.AcquireArgs()
	httpArgs.Set("form-send-request-carrier-status", carrierNewStatus)
	httpAgent.Form(httpArgs)

	httpRequest := httpAgent.Request()
	httpRequest.Header.SetMethod(fiber.MethodPatch)
	httpRequest.SetRequestURI(fmt.Sprintf("http://127.0.0.1:82/request_carrier/%d", requestId))

	httpAgent.Parse()

	response, _, err := httpAgent.Bytes()
	if err != nil {
		return err[0]
	}

	if response == fiber.StatusForbidden {
		return &UnauthorizedJWTToken{}
	} else if response == fiber.StatusUnauthorized {
		return &InvalidJWTToken{}
	}

	return nil
}

func NotifyCarrierDeleteAuthor(requestId int, token string) error {
	httpAgent := fiber.AcquireAgent()
	httpAgent.Cookie("user-jwt", token)

	httpRequest := httpAgent.Request()
	httpRequest.Header.SetMethod(fiber.MethodDelete)
	httpRequest.SetRequestURI(fmt.Sprintf("http://127.0.0.1:82/request_author/%d", requestId))

	httpAgent.Parse()

	response, _, err := httpAgent.Bytes()
	if err != nil {
		return err[0]
	}

	if response == fiber.StatusForbidden {
		return &UnauthorizedJWTToken{}
	} else if response == fiber.StatusUnauthorized {
		return &InvalidJWTToken{}
	}

	return nil
}
func NotifyCarrierDeleteCarrier(requestId int, token string) error {
	httpAgent := fiber.AcquireAgent()
	httpAgent.Cookie("user-jwt", token)

	httpRequest := httpAgent.Request()
	httpRequest.Header.SetMethod(fiber.MethodDelete)
	httpRequest.SetRequestURI(fmt.Sprintf("http://127.0.0.1:82/request_carrier/%d", requestId))

	httpAgent.Parse()

	response, _, err := httpAgent.Bytes()
	if err != nil {
		return err[0]
	}

	if response == fiber.StatusForbidden {
		return &UnauthorizedJWTToken{}
	} else if response == fiber.StatusUnauthorized {
		return &InvalidJWTToken{}
	}

	return nil
}
