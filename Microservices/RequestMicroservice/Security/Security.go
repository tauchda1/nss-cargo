package Security

import (
	"github.com/gofiber/fiber/v2"
)

func ValidateToken(token string) (bool, string, error) {
	httpAgent := fiber.AcquireAgent()
	httpAgent.Cookie("user-jwt", token)

	httpArgs := fiber.AcquireArgs()
	httpArgs.Set("user-jwt-token", token)
	httpAgent.Form(httpArgs)

	httpRequest := httpAgent.Request()
	httpRequest.Header.SetMethod(fiber.MethodPost)
	httpRequest.SetRequestURI("http://127.0.0.1:80/validate")

	httpAgent.Parse()

	response, body, err := httpAgent.Bytes()
	if err != nil {
		return false, token, err[0]
	}

	if response == fiber.StatusForbidden {
		return false, token, &UnauthorizedJWTToken{}
	} else if response == fiber.StatusUnauthorized {
		return false, token, &InvalidJWTToken{}
	} else if response == fiber.StatusOK {
		return true, string(body), nil
	}

	return false, token, nil
}
