package Db

import (
	"RequestMicroservice/Db/ent"
	entRequest "RequestMicroservice/Db/ent/request"
	"context"
)

func GetObscuredRequests() ([]*ent.Request, error) {
	client := GetConnector()
	obscuredRequests, err := client.Request.Query().All(context.Background())
	if err != nil {
		return nil, err
	}

	for index := range obscuredRequests {
		obscuredRequests[index].AddressFrom = ""
		obscuredRequests[index].AddressTo = ""
	}

	return obscuredRequests, nil
}

func GetObscuredRequest(requestId int) (request *ent.Request, err error) {
	client := GetConnector()
	obscuredRequest, err := client.Request.Query().Where(entRequest.IDEQ(requestId)).Only(context.Background())
	if err != nil {
		return nil, err
	}

	obscuredRequest.AddressFrom = ""
	obscuredRequest.AddressTo = ""

	return obscuredRequest, nil
}

func GetRequests() ([]*ent.Request, error) {
	client := GetConnector()
	requests, err := client.Request.Query().All(context.Background())
	if err != nil {
		return nil, err
	}

	return requests, nil
}

func GetRequest(requestId int) (*ent.Request, error) {
	client := GetConnector()
	request, err := client.Request.Query().Where(entRequest.IDEQ(requestId)).Only(context.Background())
	if err != nil {
		return nil, err
	}

	return request, nil
}

func CreateRequest(userId int64, description string, townFrom string, townTo string, addressFrom string, addressTo string) (*ent.Request, error) {
	client := GetConnector()
	request, err := client.Request.
		Create().
		SetUserID(userId).
		SetDescription(description).
		SetTownFrom(townFrom).
		SetTownTo(townTo).
		SetAddressFrom(addressFrom).
		SetAddressTo(addressTo).
		Save(context.Background())

	if err != nil {
		return nil, err
	}

	return request, nil
}

func UpdateRequest(requestId int, description string, townFrom string, townTo string, addressFrom string, addressTo string) (*ent.Request, error) {
	client := GetConnector()
	request, err := client.Request.
		UpdateOneID(requestId).
		SetDescription(description).
		SetTownFrom(townFrom).
		SetTownTo(townTo).
		SetAddressFrom(addressFrom).
		SetAddressTo(addressTo).
		Save(context.Background())

	if err != nil {
		return nil, err
	}

	return request, nil
}

func DeleteRequest(requestId int) error {
	client := GetConnector()
	err := client.Request.
		DeleteOneID(requestId).
		Exec(context.Background())

	if err != nil {
		return err
	}

	return nil
}
