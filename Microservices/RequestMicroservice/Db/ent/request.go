// Code generated by ent, DO NOT EDIT.

package ent

import (
	"RequestMicroservice/Db/ent/request"
	"fmt"
	"strings"

	"entgo.io/ent"
	"entgo.io/ent/dialect/sql"
)

// Request is the model entity for the Request schema.
type Request struct {
	config `json:"-"`
	// ID of the ent.
	ID int `json:"id,omitempty"`
	// UserID holds the value of the "User_Id" field.
	UserID int64 `json:"User_Id,omitempty"`
	// Description holds the value of the "Description" field.
	Description string `json:"Description,omitempty"`
	// TownFrom holds the value of the "Town_from" field.
	TownFrom string `json:"Town_from,omitempty"`
	// TownTo holds the value of the "Town_to" field.
	TownTo string `json:"Town_to,omitempty"`
	// AddressFrom holds the value of the "Address_from" field.
	AddressFrom string `json:"Address_from,omitempty"`
	// AddressTo holds the value of the "Address_to" field.
	AddressTo string `json:"Address_to,omitempty"`
	// Edges holds the relations/edges for other nodes in the graph.
	// The values are being populated by the RequestQuery when eager-loading is set.
	Edges        RequestEdges `json:"edges"`
	selectValues sql.SelectValues
}

// RequestEdges holds the relations/edges for other nodes in the graph.
type RequestEdges struct {
	// Lock holds the value of the Lock edge.
	Lock []*RequestLock `json:"Lock,omitempty"`
	// loadedTypes holds the information for reporting if a
	// type was loaded (or requested) in eager-loading or not.
	loadedTypes [1]bool
}

// LockOrErr returns the Lock value or an error if the edge
// was not loaded in eager-loading.
func (e RequestEdges) LockOrErr() ([]*RequestLock, error) {
	if e.loadedTypes[0] {
		return e.Lock, nil
	}
	return nil, &NotLoadedError{edge: "Lock"}
}

// scanValues returns the types for scanning values from sql.Rows.
func (*Request) scanValues(columns []string) ([]any, error) {
	values := make([]any, len(columns))
	for i := range columns {
		switch columns[i] {
		case request.FieldID, request.FieldUserID:
			values[i] = new(sql.NullInt64)
		case request.FieldDescription, request.FieldTownFrom, request.FieldTownTo, request.FieldAddressFrom, request.FieldAddressTo:
			values[i] = new(sql.NullString)
		default:
			values[i] = new(sql.UnknownType)
		}
	}
	return values, nil
}

// assignValues assigns the values that were returned from sql.Rows (after scanning)
// to the Request fields.
func (r *Request) assignValues(columns []string, values []any) error {
	if m, n := len(values), len(columns); m < n {
		return fmt.Errorf("mismatch number of scan values: %d != %d", m, n)
	}
	for i := range columns {
		switch columns[i] {
		case request.FieldID:
			value, ok := values[i].(*sql.NullInt64)
			if !ok {
				return fmt.Errorf("unexpected type %T for field id", value)
			}
			r.ID = int(value.Int64)
		case request.FieldUserID:
			if value, ok := values[i].(*sql.NullInt64); !ok {
				return fmt.Errorf("unexpected type %T for field User_Id", values[i])
			} else if value.Valid {
				r.UserID = value.Int64
			}
		case request.FieldDescription:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field Description", values[i])
			} else if value.Valid {
				r.Description = value.String
			}
		case request.FieldTownFrom:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field Town_from", values[i])
			} else if value.Valid {
				r.TownFrom = value.String
			}
		case request.FieldTownTo:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field Town_to", values[i])
			} else if value.Valid {
				r.TownTo = value.String
			}
		case request.FieldAddressFrom:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field Address_from", values[i])
			} else if value.Valid {
				r.AddressFrom = value.String
			}
		case request.FieldAddressTo:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field Address_to", values[i])
			} else if value.Valid {
				r.AddressTo = value.String
			}
		default:
			r.selectValues.Set(columns[i], values[i])
		}
	}
	return nil
}

// Value returns the ent.Value that was dynamically selected and assigned to the Request.
// This includes values selected through modifiers, order, etc.
func (r *Request) Value(name string) (ent.Value, error) {
	return r.selectValues.Get(name)
}

// QueryLock queries the "Lock" edge of the Request entity.
func (r *Request) QueryLock() *RequestLockQuery {
	return NewRequestClient(r.config).QueryLock(r)
}

// Update returns a builder for updating this Request.
// Note that you need to call Request.Unwrap() before calling this method if this Request
// was returned from a transaction, and the transaction was committed or rolled back.
func (r *Request) Update() *RequestUpdateOne {
	return NewRequestClient(r.config).UpdateOne(r)
}

// Unwrap unwraps the Request entity that was returned from a transaction after it was closed,
// so that all future queries will be executed through the driver which created the transaction.
func (r *Request) Unwrap() *Request {
	_tx, ok := r.config.driver.(*txDriver)
	if !ok {
		panic("ent: Request is not a transactional entity")
	}
	r.config.driver = _tx.drv
	return r
}

// String implements the fmt.Stringer.
func (r *Request) String() string {
	var builder strings.Builder
	builder.WriteString("Request(")
	builder.WriteString(fmt.Sprintf("id=%v, ", r.ID))
	builder.WriteString("User_Id=")
	builder.WriteString(fmt.Sprintf("%v", r.UserID))
	builder.WriteString(", ")
	builder.WriteString("Description=")
	builder.WriteString(r.Description)
	builder.WriteString(", ")
	builder.WriteString("Town_from=")
	builder.WriteString(r.TownFrom)
	builder.WriteString(", ")
	builder.WriteString("Town_to=")
	builder.WriteString(r.TownTo)
	builder.WriteString(", ")
	builder.WriteString("Address_from=")
	builder.WriteString(r.AddressFrom)
	builder.WriteString(", ")
	builder.WriteString("Address_to=")
	builder.WriteString(r.AddressTo)
	builder.WriteByte(')')
	return builder.String()
}

// Requests is a parsable slice of Request.
type Requests []*Request
