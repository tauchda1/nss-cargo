// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"
	"log"
	"reflect"

	"RequestMicroservice/Db/ent/migrate"

	"RequestMicroservice/Db/ent/request"
	"RequestMicroservice/Db/ent/requestlock"

	"entgo.io/ent"
	"entgo.io/ent/dialect"
	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
)

// Client is the client that holds all ent builders.
type Client struct {
	config
	// Schema is the client for creating, migrating and dropping schema.
	Schema *migrate.Schema
	// Request is the client for interacting with the Request builders.
	Request *RequestClient
	// RequestLock is the client for interacting with the RequestLock builders.
	RequestLock *RequestLockClient
}

// NewClient creates a new client configured with the given options.
func NewClient(opts ...Option) *Client {
	client := &Client{config: newConfig(opts...)}
	client.init()
	return client
}

func (c *Client) init() {
	c.Schema = migrate.NewSchema(c.driver)
	c.Request = NewRequestClient(c.config)
	c.RequestLock = NewRequestLockClient(c.config)
}

type (
	// config is the configuration for the client and its builder.
	config struct {
		// driver used for executing database requests.
		driver dialect.Driver
		// debug enable a debug logging.
		debug bool
		// log used for logging on debug mode.
		log func(...any)
		// hooks to execute on mutations.
		hooks *hooks
		// interceptors to execute on queries.
		inters *inters
	}
	// Option function to configure the client.
	Option func(*config)
)

// newConfig creates a new config for the client.
func newConfig(opts ...Option) config {
	cfg := config{log: log.Println, hooks: &hooks{}, inters: &inters{}}
	cfg.options(opts...)
	return cfg
}

// options applies the options on the config object.
func (c *config) options(opts ...Option) {
	for _, opt := range opts {
		opt(c)
	}
	if c.debug {
		c.driver = dialect.Debug(c.driver, c.log)
	}
}

// Debug enables debug logging on the ent.Driver.
func Debug() Option {
	return func(c *config) {
		c.debug = true
	}
}

// Log sets the logging function for debug mode.
func Log(fn func(...any)) Option {
	return func(c *config) {
		c.log = fn
	}
}

// Driver configures the client driver.
func Driver(driver dialect.Driver) Option {
	return func(c *config) {
		c.driver = driver
	}
}

// Open opens a database/sql.DB specified by the driver name and
// the data source name, and returns a new client attached to it.
// Optional parameters can be added for configuring the client.
func Open(driverName, dataSourceName string, options ...Option) (*Client, error) {
	switch driverName {
	case dialect.MySQL, dialect.Postgres, dialect.SQLite:
		drv, err := sql.Open(driverName, dataSourceName)
		if err != nil {
			return nil, err
		}
		return NewClient(append(options, Driver(drv))...), nil
	default:
		return nil, fmt.Errorf("unsupported driver: %q", driverName)
	}
}

// ErrTxStarted is returned when trying to start a new transaction from a transactional client.
var ErrTxStarted = errors.New("ent: cannot start a transaction within a transaction")

// Tx returns a new transactional client. The provided context
// is used until the transaction is committed or rolled back.
func (c *Client) Tx(ctx context.Context) (*Tx, error) {
	if _, ok := c.driver.(*txDriver); ok {
		return nil, ErrTxStarted
	}
	tx, err := newTx(ctx, c.driver)
	if err != nil {
		return nil, fmt.Errorf("ent: starting a transaction: %w", err)
	}
	cfg := c.config
	cfg.driver = tx
	return &Tx{
		ctx:         ctx,
		config:      cfg,
		Request:     NewRequestClient(cfg),
		RequestLock: NewRequestLockClient(cfg),
	}, nil
}

// BeginTx returns a transactional client with specified options.
func (c *Client) BeginTx(ctx context.Context, opts *sql.TxOptions) (*Tx, error) {
	if _, ok := c.driver.(*txDriver); ok {
		return nil, errors.New("ent: cannot start a transaction within a transaction")
	}
	tx, err := c.driver.(interface {
		BeginTx(context.Context, *sql.TxOptions) (dialect.Tx, error)
	}).BeginTx(ctx, opts)
	if err != nil {
		return nil, fmt.Errorf("ent: starting a transaction: %w", err)
	}
	cfg := c.config
	cfg.driver = &txDriver{tx: tx, drv: c.driver}
	return &Tx{
		ctx:         ctx,
		config:      cfg,
		Request:     NewRequestClient(cfg),
		RequestLock: NewRequestLockClient(cfg),
	}, nil
}

// Debug returns a new debug-client. It's used to get verbose logging on specific operations.
//
//	client.Debug().
//		Request.
//		Query().
//		Count(ctx)
func (c *Client) Debug() *Client {
	if c.debug {
		return c
	}
	cfg := c.config
	cfg.driver = dialect.Debug(c.driver, c.log)
	client := &Client{config: cfg}
	client.init()
	return client
}

// Close closes the database connection and prevents new queries from starting.
func (c *Client) Close() error {
	return c.driver.Close()
}

// Use adds the mutation hooks to all the entity clients.
// In order to add hooks to a specific client, call: `client.Node.Use(...)`.
func (c *Client) Use(hooks ...Hook) {
	c.Request.Use(hooks...)
	c.RequestLock.Use(hooks...)
}

// Intercept adds the query interceptors to all the entity clients.
// In order to add interceptors to a specific client, call: `client.Node.Intercept(...)`.
func (c *Client) Intercept(interceptors ...Interceptor) {
	c.Request.Intercept(interceptors...)
	c.RequestLock.Intercept(interceptors...)
}

// Mutate implements the ent.Mutator interface.
func (c *Client) Mutate(ctx context.Context, m Mutation) (Value, error) {
	switch m := m.(type) {
	case *RequestMutation:
		return c.Request.mutate(ctx, m)
	case *RequestLockMutation:
		return c.RequestLock.mutate(ctx, m)
	default:
		return nil, fmt.Errorf("ent: unknown mutation type %T", m)
	}
}

// RequestClient is a client for the Request schema.
type RequestClient struct {
	config
}

// NewRequestClient returns a client for the Request from the given config.
func NewRequestClient(c config) *RequestClient {
	return &RequestClient{config: c}
}

// Use adds a list of mutation hooks to the hooks stack.
// A call to `Use(f, g, h)` equals to `request.Hooks(f(g(h())))`.
func (c *RequestClient) Use(hooks ...Hook) {
	c.hooks.Request = append(c.hooks.Request, hooks...)
}

// Intercept adds a list of query interceptors to the interceptors stack.
// A call to `Intercept(f, g, h)` equals to `request.Intercept(f(g(h())))`.
func (c *RequestClient) Intercept(interceptors ...Interceptor) {
	c.inters.Request = append(c.inters.Request, interceptors...)
}

// Create returns a builder for creating a Request entity.
func (c *RequestClient) Create() *RequestCreate {
	mutation := newRequestMutation(c.config, OpCreate)
	return &RequestCreate{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// CreateBulk returns a builder for creating a bulk of Request entities.
func (c *RequestClient) CreateBulk(builders ...*RequestCreate) *RequestCreateBulk {
	return &RequestCreateBulk{config: c.config, builders: builders}
}

// MapCreateBulk creates a bulk creation builder from the given slice. For each item in the slice, the function creates
// a builder and applies setFunc on it.
func (c *RequestClient) MapCreateBulk(slice any, setFunc func(*RequestCreate, int)) *RequestCreateBulk {
	rv := reflect.ValueOf(slice)
	if rv.Kind() != reflect.Slice {
		return &RequestCreateBulk{err: fmt.Errorf("calling to RequestClient.MapCreateBulk with wrong type %T, need slice", slice)}
	}
	builders := make([]*RequestCreate, rv.Len())
	for i := 0; i < rv.Len(); i++ {
		builders[i] = c.Create()
		setFunc(builders[i], i)
	}
	return &RequestCreateBulk{config: c.config, builders: builders}
}

// Update returns an update builder for Request.
func (c *RequestClient) Update() *RequestUpdate {
	mutation := newRequestMutation(c.config, OpUpdate)
	return &RequestUpdate{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// UpdateOne returns an update builder for the given entity.
func (c *RequestClient) UpdateOne(r *Request) *RequestUpdateOne {
	mutation := newRequestMutation(c.config, OpUpdateOne, withRequest(r))
	return &RequestUpdateOne{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// UpdateOneID returns an update builder for the given id.
func (c *RequestClient) UpdateOneID(id int) *RequestUpdateOne {
	mutation := newRequestMutation(c.config, OpUpdateOne, withRequestID(id))
	return &RequestUpdateOne{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// Delete returns a delete builder for Request.
func (c *RequestClient) Delete() *RequestDelete {
	mutation := newRequestMutation(c.config, OpDelete)
	return &RequestDelete{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// DeleteOne returns a builder for deleting the given entity.
func (c *RequestClient) DeleteOne(r *Request) *RequestDeleteOne {
	return c.DeleteOneID(r.ID)
}

// DeleteOneID returns a builder for deleting the given entity by its id.
func (c *RequestClient) DeleteOneID(id int) *RequestDeleteOne {
	builder := c.Delete().Where(request.ID(id))
	builder.mutation.id = &id
	builder.mutation.op = OpDeleteOne
	return &RequestDeleteOne{builder}
}

// Query returns a query builder for Request.
func (c *RequestClient) Query() *RequestQuery {
	return &RequestQuery{
		config: c.config,
		ctx:    &QueryContext{Type: TypeRequest},
		inters: c.Interceptors(),
	}
}

// Get returns a Request entity by its id.
func (c *RequestClient) Get(ctx context.Context, id int) (*Request, error) {
	return c.Query().Where(request.ID(id)).Only(ctx)
}

// GetX is like Get, but panics if an error occurs.
func (c *RequestClient) GetX(ctx context.Context, id int) *Request {
	obj, err := c.Get(ctx, id)
	if err != nil {
		panic(err)
	}
	return obj
}

// QueryLock queries the Lock edge of a Request.
func (c *RequestClient) QueryLock(r *Request) *RequestLockQuery {
	query := (&RequestLockClient{config: c.config}).Query()
	query.path = func(context.Context) (fromV *sql.Selector, _ error) {
		id := r.ID
		step := sqlgraph.NewStep(
			sqlgraph.From(request.Table, request.FieldID, id),
			sqlgraph.To(requestlock.Table, requestlock.FieldID),
			sqlgraph.Edge(sqlgraph.O2M, false, request.LockTable, request.LockColumn),
		)
		fromV = sqlgraph.Neighbors(r.driver.Dialect(), step)
		return fromV, nil
	}
	return query
}

// Hooks returns the client hooks.
func (c *RequestClient) Hooks() []Hook {
	return c.hooks.Request
}

// Interceptors returns the client interceptors.
func (c *RequestClient) Interceptors() []Interceptor {
	return c.inters.Request
}

func (c *RequestClient) mutate(ctx context.Context, m *RequestMutation) (Value, error) {
	switch m.Op() {
	case OpCreate:
		return (&RequestCreate{config: c.config, hooks: c.Hooks(), mutation: m}).Save(ctx)
	case OpUpdate:
		return (&RequestUpdate{config: c.config, hooks: c.Hooks(), mutation: m}).Save(ctx)
	case OpUpdateOne:
		return (&RequestUpdateOne{config: c.config, hooks: c.Hooks(), mutation: m}).Save(ctx)
	case OpDelete, OpDeleteOne:
		return (&RequestDelete{config: c.config, hooks: c.Hooks(), mutation: m}).Exec(ctx)
	default:
		return nil, fmt.Errorf("ent: unknown Request mutation op: %q", m.Op())
	}
}

// RequestLockClient is a client for the RequestLock schema.
type RequestLockClient struct {
	config
}

// NewRequestLockClient returns a client for the RequestLock from the given config.
func NewRequestLockClient(c config) *RequestLockClient {
	return &RequestLockClient{config: c}
}

// Use adds a list of mutation hooks to the hooks stack.
// A call to `Use(f, g, h)` equals to `requestlock.Hooks(f(g(h())))`.
func (c *RequestLockClient) Use(hooks ...Hook) {
	c.hooks.RequestLock = append(c.hooks.RequestLock, hooks...)
}

// Intercept adds a list of query interceptors to the interceptors stack.
// A call to `Intercept(f, g, h)` equals to `requestlock.Intercept(f(g(h())))`.
func (c *RequestLockClient) Intercept(interceptors ...Interceptor) {
	c.inters.RequestLock = append(c.inters.RequestLock, interceptors...)
}

// Create returns a builder for creating a RequestLock entity.
func (c *RequestLockClient) Create() *RequestLockCreate {
	mutation := newRequestLockMutation(c.config, OpCreate)
	return &RequestLockCreate{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// CreateBulk returns a builder for creating a bulk of RequestLock entities.
func (c *RequestLockClient) CreateBulk(builders ...*RequestLockCreate) *RequestLockCreateBulk {
	return &RequestLockCreateBulk{config: c.config, builders: builders}
}

// MapCreateBulk creates a bulk creation builder from the given slice. For each item in the slice, the function creates
// a builder and applies setFunc on it.
func (c *RequestLockClient) MapCreateBulk(slice any, setFunc func(*RequestLockCreate, int)) *RequestLockCreateBulk {
	rv := reflect.ValueOf(slice)
	if rv.Kind() != reflect.Slice {
		return &RequestLockCreateBulk{err: fmt.Errorf("calling to RequestLockClient.MapCreateBulk with wrong type %T, need slice", slice)}
	}
	builders := make([]*RequestLockCreate, rv.Len())
	for i := 0; i < rv.Len(); i++ {
		builders[i] = c.Create()
		setFunc(builders[i], i)
	}
	return &RequestLockCreateBulk{config: c.config, builders: builders}
}

// Update returns an update builder for RequestLock.
func (c *RequestLockClient) Update() *RequestLockUpdate {
	mutation := newRequestLockMutation(c.config, OpUpdate)
	return &RequestLockUpdate{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// UpdateOne returns an update builder for the given entity.
func (c *RequestLockClient) UpdateOne(rl *RequestLock) *RequestLockUpdateOne {
	mutation := newRequestLockMutation(c.config, OpUpdateOne, withRequestLock(rl))
	return &RequestLockUpdateOne{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// UpdateOneID returns an update builder for the given id.
func (c *RequestLockClient) UpdateOneID(id int) *RequestLockUpdateOne {
	mutation := newRequestLockMutation(c.config, OpUpdateOne, withRequestLockID(id))
	return &RequestLockUpdateOne{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// Delete returns a delete builder for RequestLock.
func (c *RequestLockClient) Delete() *RequestLockDelete {
	mutation := newRequestLockMutation(c.config, OpDelete)
	return &RequestLockDelete{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// DeleteOne returns a builder for deleting the given entity.
func (c *RequestLockClient) DeleteOne(rl *RequestLock) *RequestLockDeleteOne {
	return c.DeleteOneID(rl.ID)
}

// DeleteOneID returns a builder for deleting the given entity by its id.
func (c *RequestLockClient) DeleteOneID(id int) *RequestLockDeleteOne {
	builder := c.Delete().Where(requestlock.ID(id))
	builder.mutation.id = &id
	builder.mutation.op = OpDeleteOne
	return &RequestLockDeleteOne{builder}
}

// Query returns a query builder for RequestLock.
func (c *RequestLockClient) Query() *RequestLockQuery {
	return &RequestLockQuery{
		config: c.config,
		ctx:    &QueryContext{Type: TypeRequestLock},
		inters: c.Interceptors(),
	}
}

// Get returns a RequestLock entity by its id.
func (c *RequestLockClient) Get(ctx context.Context, id int) (*RequestLock, error) {
	return c.Query().Where(requestlock.ID(id)).Only(ctx)
}

// GetX is like Get, but panics if an error occurs.
func (c *RequestLockClient) GetX(ctx context.Context, id int) *RequestLock {
	obj, err := c.Get(ctx, id)
	if err != nil {
		panic(err)
	}
	return obj
}

// QueryRequest queries the Request edge of a RequestLock.
func (c *RequestLockClient) QueryRequest(rl *RequestLock) *RequestQuery {
	query := (&RequestClient{config: c.config}).Query()
	query.path = func(context.Context) (fromV *sql.Selector, _ error) {
		id := rl.ID
		step := sqlgraph.NewStep(
			sqlgraph.From(requestlock.Table, requestlock.FieldID, id),
			sqlgraph.To(request.Table, request.FieldID),
			sqlgraph.Edge(sqlgraph.M2O, true, requestlock.RequestTable, requestlock.RequestColumn),
		)
		fromV = sqlgraph.Neighbors(rl.driver.Dialect(), step)
		return fromV, nil
	}
	return query
}

// Hooks returns the client hooks.
func (c *RequestLockClient) Hooks() []Hook {
	return c.hooks.RequestLock
}

// Interceptors returns the client interceptors.
func (c *RequestLockClient) Interceptors() []Interceptor {
	return c.inters.RequestLock
}

func (c *RequestLockClient) mutate(ctx context.Context, m *RequestLockMutation) (Value, error) {
	switch m.Op() {
	case OpCreate:
		return (&RequestLockCreate{config: c.config, hooks: c.Hooks(), mutation: m}).Save(ctx)
	case OpUpdate:
		return (&RequestLockUpdate{config: c.config, hooks: c.Hooks(), mutation: m}).Save(ctx)
	case OpUpdateOne:
		return (&RequestLockUpdateOne{config: c.config, hooks: c.Hooks(), mutation: m}).Save(ctx)
	case OpDelete, OpDeleteOne:
		return (&RequestLockDelete{config: c.config, hooks: c.Hooks(), mutation: m}).Exec(ctx)
	default:
		return nil, fmt.Errorf("ent: unknown RequestLock mutation op: %q", m.Op())
	}
}

// hooks and interceptors per client, for fast access.
type (
	hooks struct {
		Request, RequestLock []ent.Hook
	}
	inters struct {
		Request, RequestLock []ent.Interceptor
	}
)
