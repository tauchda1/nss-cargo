package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// RequestLock holds the schema definition for the RequestLock entity.
type RequestLock struct {
	ent.Schema
}

// Fields of the RequestLock.
func (RequestLock) Fields() []ent.Field {
	return []ent.Field{
		field.Int("User_Id"),
	}
}

// Edges of the RequestLock.
func (RequestLock) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("Request", Request.Type).
			Ref("Lock").
			Unique().
			Required(),
	}
}
