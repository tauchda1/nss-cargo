package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// Request holds the schema definition for the Request entity.
type Request struct {
	ent.Schema
}

// Fields of the Request.
func (Request) Fields() []ent.Field {
	return []ent.Field{
		field.Int64("User_Id"),
		field.Text("Description").NotEmpty(),
		field.String("Town_from").NotEmpty(),
		field.String("Town_to").NotEmpty(),
		field.String("Address_from").NotEmpty(),
		field.String("Address_to").NotEmpty(),
	}
}

// Edges of the Request.
func (Request) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("Lock", RequestLock.Type).
			Annotations(entsql.OnDelete(entsql.Cascade)),
	}
}
