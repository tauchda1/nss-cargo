// Code generated by ent, DO NOT EDIT.

package ent

import (
	"RequestMicroservice/Db/ent/request"
	"RequestMicroservice/Db/ent/requestlock"
	"context"
	"errors"
	"fmt"

	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// RequestLockCreate is the builder for creating a RequestLock entity.
type RequestLockCreate struct {
	config
	mutation *RequestLockMutation
	hooks    []Hook
}

// SetUserID sets the "User_Id" field.
func (rlc *RequestLockCreate) SetUserID(i int) *RequestLockCreate {
	rlc.mutation.SetUserID(i)
	return rlc
}

// SetRequestID sets the "Request" edge to the Request entity by ID.
func (rlc *RequestLockCreate) SetRequestID(id int) *RequestLockCreate {
	rlc.mutation.SetRequestID(id)
	return rlc
}

// SetRequest sets the "Request" edge to the Request entity.
func (rlc *RequestLockCreate) SetRequest(r *Request) *RequestLockCreate {
	return rlc.SetRequestID(r.ID)
}

// Mutation returns the RequestLockMutation object of the builder.
func (rlc *RequestLockCreate) Mutation() *RequestLockMutation {
	return rlc.mutation
}

// Save creates the RequestLock in the database.
func (rlc *RequestLockCreate) Save(ctx context.Context) (*RequestLock, error) {
	return withHooks(ctx, rlc.sqlSave, rlc.mutation, rlc.hooks)
}

// SaveX calls Save and panics if Save returns an error.
func (rlc *RequestLockCreate) SaveX(ctx context.Context) *RequestLock {
	v, err := rlc.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (rlc *RequestLockCreate) Exec(ctx context.Context) error {
	_, err := rlc.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (rlc *RequestLockCreate) ExecX(ctx context.Context) {
	if err := rlc.Exec(ctx); err != nil {
		panic(err)
	}
}

// check runs all checks and user-defined validators on the builder.
func (rlc *RequestLockCreate) check() error {
	if _, ok := rlc.mutation.UserID(); !ok {
		return &ValidationError{Name: "User_Id", err: errors.New(`ent: missing required field "RequestLock.User_Id"`)}
	}
	if _, ok := rlc.mutation.RequestID(); !ok {
		return &ValidationError{Name: "Request", err: errors.New(`ent: missing required edge "RequestLock.Request"`)}
	}
	return nil
}

func (rlc *RequestLockCreate) sqlSave(ctx context.Context) (*RequestLock, error) {
	if err := rlc.check(); err != nil {
		return nil, err
	}
	_node, _spec := rlc.createSpec()
	if err := sqlgraph.CreateNode(ctx, rlc.driver, _spec); err != nil {
		if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return nil, err
	}
	id := _spec.ID.Value.(int64)
	_node.ID = int(id)
	rlc.mutation.id = &_node.ID
	rlc.mutation.done = true
	return _node, nil
}

func (rlc *RequestLockCreate) createSpec() (*RequestLock, *sqlgraph.CreateSpec) {
	var (
		_node = &RequestLock{config: rlc.config}
		_spec = sqlgraph.NewCreateSpec(requestlock.Table, sqlgraph.NewFieldSpec(requestlock.FieldID, field.TypeInt))
	)
	if value, ok := rlc.mutation.UserID(); ok {
		_spec.SetField(requestlock.FieldUserID, field.TypeInt, value)
		_node.UserID = value
	}
	if nodes := rlc.mutation.RequestIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   requestlock.RequestTable,
			Columns: []string{requestlock.RequestColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: sqlgraph.NewFieldSpec(request.FieldID, field.TypeInt),
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_node.request_lock = &nodes[0]
		_spec.Edges = append(_spec.Edges, edge)
	}
	return _node, _spec
}

// RequestLockCreateBulk is the builder for creating many RequestLock entities in bulk.
type RequestLockCreateBulk struct {
	config
	err      error
	builders []*RequestLockCreate
}

// Save creates the RequestLock entities in the database.
func (rlcb *RequestLockCreateBulk) Save(ctx context.Context) ([]*RequestLock, error) {
	if rlcb.err != nil {
		return nil, rlcb.err
	}
	specs := make([]*sqlgraph.CreateSpec, len(rlcb.builders))
	nodes := make([]*RequestLock, len(rlcb.builders))
	mutators := make([]Mutator, len(rlcb.builders))
	for i := range rlcb.builders {
		func(i int, root context.Context) {
			builder := rlcb.builders[i]
			var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
				mutation, ok := m.(*RequestLockMutation)
				if !ok {
					return nil, fmt.Errorf("unexpected mutation type %T", m)
				}
				if err := builder.check(); err != nil {
					return nil, err
				}
				builder.mutation = mutation
				var err error
				nodes[i], specs[i] = builder.createSpec()
				if i < len(mutators)-1 {
					_, err = mutators[i+1].Mutate(root, rlcb.builders[i+1].mutation)
				} else {
					spec := &sqlgraph.BatchCreateSpec{Nodes: specs}
					// Invoke the actual operation on the latest mutation in the chain.
					if err = sqlgraph.BatchCreate(ctx, rlcb.driver, spec); err != nil {
						if sqlgraph.IsConstraintError(err) {
							err = &ConstraintError{msg: err.Error(), wrap: err}
						}
					}
				}
				if err != nil {
					return nil, err
				}
				mutation.id = &nodes[i].ID
				if specs[i].ID.Value != nil {
					id := specs[i].ID.Value.(int64)
					nodes[i].ID = int(id)
				}
				mutation.done = true
				return nodes[i], nil
			})
			for i := len(builder.hooks) - 1; i >= 0; i-- {
				mut = builder.hooks[i](mut)
			}
			mutators[i] = mut
		}(i, ctx)
	}
	if len(mutators) > 0 {
		if _, err := mutators[0].Mutate(ctx, rlcb.builders[0].mutation); err != nil {
			return nil, err
		}
	}
	return nodes, nil
}

// SaveX is like Save, but panics if an error occurs.
func (rlcb *RequestLockCreateBulk) SaveX(ctx context.Context) []*RequestLock {
	v, err := rlcb.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (rlcb *RequestLockCreateBulk) Exec(ctx context.Context) error {
	_, err := rlcb.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (rlcb *RequestLockCreateBulk) ExecX(ctx context.Context) {
	if err := rlcb.Exec(ctx); err != nil {
		panic(err)
	}
}
