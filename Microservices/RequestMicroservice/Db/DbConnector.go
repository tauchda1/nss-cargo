package Db

import (
	"RequestMicroservice/Db/ent"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"os"
)

var connector *ent.Client = nil

func InitConnector(db string) {
	var err error
	if connector == nil {
		connector, err = ent.Open("mysql", fmt.Sprintf("%s:%s@tcp(localhost:3306)/%s?parseTime=True", os.Getenv("database_user"), os.Getenv("database_password"), db))
		if err != nil {
			panic(err)
		}
	}
}

func GetConnector() *ent.Client {
	if connector == nil {
		InitConnector("request_db")
	}

	return connector
}

func DestroyConnector() {
	connector.Close()
}
