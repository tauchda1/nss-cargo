package Db

import (
	"RequestMicroservice/Db/ent"
	"RequestMicroservice/Db/ent/request"
	"RequestMicroservice/Db/ent/requestlock"
	"context"
)

func CreateLock(request *ent.Request, userId int) (*ent.RequestLock, error) {
	client := GetConnector()

	lock, err := client.RequestLock.Create().SetRequest(request).SetUserID(userId).Save(context.Background())

	if err != nil {
		return nil, err
	}

	return lock, nil
}

func GetUserLocks(userId int) ([]*ent.RequestLock, error) {
	client := GetConnector()

	locks, err := client.RequestLock.Query().Where(requestlock.UserIDEQ(userId)).All(context.Background())
	if err != nil {
		return nil, err
	}

	return locks, nil
}

func GetRequestLock(requestId int) (*ent.RequestLock, error) {
	client := GetConnector()

	lock, err := client.RequestLock.Query().Where(requestlock.HasRequestWith(request.IDEQ(requestId))).Only(context.Background())

	if err != nil {
		return nil, err
	}

	return lock, nil
}

func DeleteLock(requestLock *ent.RequestLock) error {
	client := GetConnector()

	err := client.RequestLock.DeleteOne(requestLock).Exec(context.Background())
	if err != nil {
		return err
	}

	return nil
}
