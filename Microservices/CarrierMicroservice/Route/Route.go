package Route

import (
	"CarrierMicroservice/Db"
	"CarrierMicroservice/Security"
	"errors"
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v5"
	"os"
	"strconv"
)

const (
	OutputRequestsName = "all-requests"
	OutputRequestName  = "out-request"
)

func SetupRoutes(app *fiber.App) {
	app.Use(validateToken)
	app.Use(unpackToken)

	_ = app.Get("/request", getUserRequests)
	_ = app.Get("/request/:request_id", getRequest)

	_ = app.Post("/request", createNewRequest)

	_ = app.Patch("/request_author/:request_id", updateAuthorRequestStatus)
	_ = app.Patch("/request_carrier/:request_id", updateCarrierRequestStatus)

	_ = app.Delete("/request_author/:request_id", deleteRequestAuthor)
	_ = app.Delete("/request_carrier/:request_id", deleteRequestCarrier)
}

// region Middleware
func validateToken(ctx *fiber.Ctx) error {
	token := ctx.Cookies("user-jwt", "")
	valid, token, err := Security.ValidateToken(token)
	if !valid {
		if errors.Is(err, &Security.InvalidJWTToken{}) {
			return ctx.SendStatus(fiber.StatusUnauthorized)
		} else if errors.Is(err, &Security.UnauthorizedJWTToken{}) {
			return ctx.SendStatus(fiber.StatusForbidden)
		}

		return ctx.SendStatus(480)
	}

	cookie := new(fiber.Cookie)
	cookie.Name = "user-jwt"
	cookie.Value = token

	ctx.Cookie(cookie)

	return ctx.Next()
}

func unpackToken(ctx *fiber.Ctx) error {
	token := ctx.Cookies("user-jwt", "")
	parsedToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) { return []byte(os.Getenv("JWT_SECRET")), nil })
	if err != nil {
		return ctx.SendStatus(fiber.StatusBadRequest)
	}

	claims := parsedToken.Claims.(jwt.MapClaims)

	ctx.Locals("userId", int(claims["userId"].(float64))) // ... strictly typed languages problems
	ctx.Locals("userType", claims["userType"])

	return ctx.Next()
}

//endregion

// region Requests routes
func getRequest(ctx *fiber.Ctx) error {
	requestIdString := ctx.Params("request_id")
	requestId, err := strconv.Atoi(requestIdString)
	if err != nil {
		return ctx.SendStatus(fiber.StatusBadRequest)
	}

	request, err := Db.GetCarrierRequest(requestId)
	if err != nil {
		return ctx.SendStatus(fiber.StatusNotFound)
	}

	if request.CarrierID != ctx.Locals("userId").(int) {
		return ctx.SendStatus(fiber.StatusForbidden)
	}

	output := make(map[string]any)

	output[OutputRequestName] = request

	return ctx.JSON(output)
}

func getUserRequests(ctx *fiber.Ctx) error {
	requests, err := Db.GetUsersCarrierRequests(ctx.Locals("userId").(int))
	if err != nil {
		return ctx.SendStatus(fiber.StatusNotFound)
	}

	output := make(map[string]any)

	output[OutputRequestsName] = requests

	return ctx.JSON(output)
}

func createNewRequest(ctx *fiber.Ctx) error {
	requestId, err := strconv.Atoi(ctx.FormValue("form-send-request-requestId"))
	if err != nil {
		return ctx.SendStatus(fiber.StatusBadRequest)
	}

	authorId, err := strconv.Atoi(ctx.FormValue("form-send-request-authorId"))
	if err != nil {
		return ctx.SendStatus(fiber.StatusBadRequest)
	}

	carrierId, err := strconv.Atoi(ctx.FormValue("form-send-request-carrierId"))
	if err != nil {
		return ctx.SendStatus(fiber.StatusBadRequest)
	}

	request, err := Db.CreateRequest(
		requestId,
		authorId,
		carrierId,
		ctx.FormValue("form-send-request-description"),
		ctx.FormValue("form-send-request-town-from"),
		ctx.FormValue("form-send-request-town-to"),
		ctx.FormValue("form-send-request-address-from"),
		ctx.FormValue("form-send-request-address-to"),
	)

	if err != nil {
		return ctx.SendStatus(fiber.StatusBadRequest)
	}

	ctx.Status(fiber.StatusCreated)
	return ctx.JSON(request)
}

func updateAuthorRequestStatus(ctx *fiber.Ctx) error {
	requestIdString := ctx.Params("request_id")
	requestId, err := strconv.Atoi(requestIdString)
	if err != nil {
		return ctx.SendStatus(fiber.StatusBadRequest)
	}

	request, err := Db.GetCarrierRequest(requestId)
	if err != nil || request == nil {
		return ctx.SendStatus(fiber.StatusNotFound)
	}

	if request.AuthorID != ctx.Locals("userId").(int) && request.CarrierID != ctx.Locals("userId") {
		return ctx.SendStatus(fiber.StatusForbidden)
	}

	err = Db.UpdateRequestStatus(
		requestId,
		ctx.FormValue("form-send-request-author-status"),
		request.CarrierRequestStatus.String(),
	)

	if err != nil {
		return ctx.SendStatus(fiber.StatusInternalServerError)
	}

	ctx.Status(fiber.StatusOK)
	return ctx.JSON(request)
}

func updateCarrierRequestStatus(ctx *fiber.Ctx) error {
	requestIdString := ctx.Params("request_id")
	requestId, err := strconv.Atoi(requestIdString)
	if err != nil {
		return ctx.SendStatus(fiber.StatusBadRequest)
	}

	request, err := Db.GetCarrierRequest(requestId)
	if err != nil || request == nil {
		return ctx.SendStatus(fiber.StatusNotFound)
	}

	if request.AuthorID != ctx.Locals("userId").(int) && request.CarrierID != ctx.Locals("userId") {
		return ctx.SendStatus(fiber.StatusForbidden)
	}

	err = Db.UpdateRequestStatus(
		requestId,
		request.AutorRequestStatus.String(),
		ctx.FormValue("form-send-request-carrier-status"),
	)

	if err != nil {
		return ctx.SendStatus(fiber.StatusInternalServerError)
	}

	ctx.Status(fiber.StatusOK)
	return ctx.JSON(request)
}

func deleteRequestAuthor(ctx *fiber.Ctx) error {
	requestIdString := ctx.Params("request_id")
	requestId, err := strconv.Atoi(requestIdString)
	if err != nil {
		return ctx.SendStatus(fiber.StatusBadRequest)
	}

	request, err := Db.GetCarrierRequest(requestId)
	if err != nil {
		return ctx.SendStatus(fiber.StatusNotFound)
	}

	if request.AuthorID != ctx.Locals("userId").(int) && request.CarrierID != ctx.Locals("userId").(int) {
		return ctx.SendStatus(fiber.StatusForbidden)
	}
	err = Db.UpdateRequestStatus(requestId, "Deleted", "Deleted")
	if err != nil {
		return ctx.SendStatus(fiber.StatusInternalServerError)
	}

	return ctx.SendStatus(fiber.StatusNoContent)
}

func deleteRequestCarrier(ctx *fiber.Ctx) error {
	requestIdString := ctx.Params("request_id")
	requestId, err := strconv.Atoi(requestIdString)
	if err != nil {
		return ctx.SendStatus(fiber.StatusBadRequest)
	}

	request, err := Db.GetCarrierRequest(requestId)
	if err != nil {
		return ctx.SendStatus(fiber.StatusNotFound)
	}

	if request.AuthorID != ctx.Locals("userId").(int) && request.CarrierID != ctx.Locals("userId").(int) {
		return ctx.SendStatus(fiber.StatusForbidden)
	}
	err = Db.UpdateRequestStatus(requestId, "Pending", "Deleted")
	if err != nil {
		return ctx.SendStatus(fiber.StatusInternalServerError)
	}

	return ctx.SendStatus(fiber.StatusNoContent)
}

//endregion
