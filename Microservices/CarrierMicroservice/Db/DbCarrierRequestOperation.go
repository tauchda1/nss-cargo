package Db

import (
	"CarrierMicroservice/Db/ent"
	entCarrierRequest "CarrierMicroservice/Db/ent/carrierrequest"
	"context"
)

func GetCarrierRequest(requestId int) (*ent.CarrierRequest, error) {
	client := GetConnector()
	requests, err := client.CarrierRequest.Query().Where(entCarrierRequest.RequestIDEQ(requestId)).All(context.Background())
	if err != nil {
		return nil, err
	}

	if len(requests) == 0 {
		return nil, nil
	}

	request := requests[len(requests)-1]

	return request, nil
}

func GetUsersCarrierRequests(userId int) ([]*ent.CarrierRequest, error) {
	client := GetConnector()
	requests, err := client.CarrierRequest.Query().Where(entCarrierRequest.CarrierIDEQ(userId)).All(context.Background())
	if err != nil {
		return nil, err
	}

	return requests, nil
}

func CreateRequest(requestId int, authorId int, carrierId int, description string, townFrom string, townTo string, addressFrom string, addressTo string) (*ent.CarrierRequest, error) {
	client := GetConnector()
	request, err := client.CarrierRequest.
		Create().
		SetRequestID(requestId).
		SetAuthorID(authorId).
		SetCarrierID(carrierId).
		SetDescription(description).
		SetTownFrom(townFrom).
		SetTownTo(townTo).
		SetAddressFrom(addressFrom).
		SetAddressTo(addressTo).
		Save(context.Background())

	if err != nil {
		return nil, err
	}

	return request, nil
}

func UpdateRequestStatus(requestId int, updatedStatusAuthorString string, updatedStatusCarrierString string) error {
	client := GetConnector()
	updatedStatusAuthor := entCarrierRequest.AutorRequestStatus(updatedStatusAuthorString)
	updatedStatusCarrier := entCarrierRequest.CarrierRequestStatus(updatedStatusCarrierString)
	request, err := GetCarrierRequest(requestId)
	if err != nil {
		return err
	}
	_, err = client.CarrierRequest.UpdateOne(request).SetAutorRequestStatus(updatedStatusAuthor).SetCarrierRequestStatus(updatedStatusCarrier).Save(context.Background())

	return err
}
