package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
)

// CarrierRequest holds the schema definition for the CarrierRequest entity.
type CarrierRequest struct {
	ent.Schema
}

// Fields of the CarrierRequest.
func (CarrierRequest) Fields() []ent.Field {
	return []ent.Field{
		field.Int("Request_Id"),
		field.Int("Author_Id"),
		field.Int("Carrier_Id"),
		field.Text("Description"),
		field.String("Town_from"),
		field.String("Town_to"),
		field.String("Address_from"),
		field.String("Address_to"),
		field.Enum("Autor_request_status").
			Values("Pending", "Running", "Completed", "Request changed", "Deleted").
			Default("Pending"),
		field.Enum("Carrier_request_status").
			Values("Pending", "Running", "Completed", "Request changed", "Deleted").
			Default("Pending"),
	}
}

// Edges of the CarrierRequest.
func (CarrierRequest) Edges() []ent.Edge {
	return nil
}
