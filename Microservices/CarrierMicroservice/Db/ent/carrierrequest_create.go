// Code generated by ent, DO NOT EDIT.

package ent

import (
	"CarrierMicroservice/Db/ent/carrierrequest"
	"context"
	"errors"
	"fmt"

	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// CarrierRequestCreate is the builder for creating a CarrierRequest entity.
type CarrierRequestCreate struct {
	config
	mutation *CarrierRequestMutation
	hooks    []Hook
}

// SetRequestID sets the "Request_Id" field.
func (crc *CarrierRequestCreate) SetRequestID(i int) *CarrierRequestCreate {
	crc.mutation.SetRequestID(i)
	return crc
}

// SetAuthorID sets the "Author_Id" field.
func (crc *CarrierRequestCreate) SetAuthorID(i int) *CarrierRequestCreate {
	crc.mutation.SetAuthorID(i)
	return crc
}

// SetCarrierID sets the "Carrier_Id" field.
func (crc *CarrierRequestCreate) SetCarrierID(i int) *CarrierRequestCreate {
	crc.mutation.SetCarrierID(i)
	return crc
}

// SetDescription sets the "Description" field.
func (crc *CarrierRequestCreate) SetDescription(s string) *CarrierRequestCreate {
	crc.mutation.SetDescription(s)
	return crc
}

// SetTownFrom sets the "Town_from" field.
func (crc *CarrierRequestCreate) SetTownFrom(s string) *CarrierRequestCreate {
	crc.mutation.SetTownFrom(s)
	return crc
}

// SetTownTo sets the "Town_to" field.
func (crc *CarrierRequestCreate) SetTownTo(s string) *CarrierRequestCreate {
	crc.mutation.SetTownTo(s)
	return crc
}

// SetAddressFrom sets the "Address_from" field.
func (crc *CarrierRequestCreate) SetAddressFrom(s string) *CarrierRequestCreate {
	crc.mutation.SetAddressFrom(s)
	return crc
}

// SetAddressTo sets the "Address_to" field.
func (crc *CarrierRequestCreate) SetAddressTo(s string) *CarrierRequestCreate {
	crc.mutation.SetAddressTo(s)
	return crc
}

// SetAutorRequestStatus sets the "Autor_request_status" field.
func (crc *CarrierRequestCreate) SetAutorRequestStatus(crs carrierrequest.AutorRequestStatus) *CarrierRequestCreate {
	crc.mutation.SetAutorRequestStatus(crs)
	return crc
}

// SetNillableAutorRequestStatus sets the "Autor_request_status" field if the given value is not nil.
func (crc *CarrierRequestCreate) SetNillableAutorRequestStatus(crs *carrierrequest.AutorRequestStatus) *CarrierRequestCreate {
	if crs != nil {
		crc.SetAutorRequestStatus(*crs)
	}
	return crc
}

// SetCarrierRequestStatus sets the "Carrier_request_status" field.
func (crc *CarrierRequestCreate) SetCarrierRequestStatus(crs carrierrequest.CarrierRequestStatus) *CarrierRequestCreate {
	crc.mutation.SetCarrierRequestStatus(crs)
	return crc
}

// SetNillableCarrierRequestStatus sets the "Carrier_request_status" field if the given value is not nil.
func (crc *CarrierRequestCreate) SetNillableCarrierRequestStatus(crs *carrierrequest.CarrierRequestStatus) *CarrierRequestCreate {
	if crs != nil {
		crc.SetCarrierRequestStatus(*crs)
	}
	return crc
}

// Mutation returns the CarrierRequestMutation object of the builder.
func (crc *CarrierRequestCreate) Mutation() *CarrierRequestMutation {
	return crc.mutation
}

// Save creates the CarrierRequest in the database.
func (crc *CarrierRequestCreate) Save(ctx context.Context) (*CarrierRequest, error) {
	crc.defaults()
	return withHooks(ctx, crc.sqlSave, crc.mutation, crc.hooks)
}

// SaveX calls Save and panics if Save returns an error.
func (crc *CarrierRequestCreate) SaveX(ctx context.Context) *CarrierRequest {
	v, err := crc.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (crc *CarrierRequestCreate) Exec(ctx context.Context) error {
	_, err := crc.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (crc *CarrierRequestCreate) ExecX(ctx context.Context) {
	if err := crc.Exec(ctx); err != nil {
		panic(err)
	}
}

// defaults sets the default values of the builder before save.
func (crc *CarrierRequestCreate) defaults() {
	if _, ok := crc.mutation.AutorRequestStatus(); !ok {
		v := carrierrequest.DefaultAutorRequestStatus
		crc.mutation.SetAutorRequestStatus(v)
	}
	if _, ok := crc.mutation.CarrierRequestStatus(); !ok {
		v := carrierrequest.DefaultCarrierRequestStatus
		crc.mutation.SetCarrierRequestStatus(v)
	}
}

// check runs all checks and user-defined validators on the builder.
func (crc *CarrierRequestCreate) check() error {
	if _, ok := crc.mutation.RequestID(); !ok {
		return &ValidationError{Name: "Request_Id", err: errors.New(`ent: missing required field "CarrierRequest.Request_Id"`)}
	}
	if _, ok := crc.mutation.AuthorID(); !ok {
		return &ValidationError{Name: "Author_Id", err: errors.New(`ent: missing required field "CarrierRequest.Author_Id"`)}
	}
	if _, ok := crc.mutation.CarrierID(); !ok {
		return &ValidationError{Name: "Carrier_Id", err: errors.New(`ent: missing required field "CarrierRequest.Carrier_Id"`)}
	}
	if _, ok := crc.mutation.Description(); !ok {
		return &ValidationError{Name: "Description", err: errors.New(`ent: missing required field "CarrierRequest.Description"`)}
	}
	if _, ok := crc.mutation.TownFrom(); !ok {
		return &ValidationError{Name: "Town_from", err: errors.New(`ent: missing required field "CarrierRequest.Town_from"`)}
	}
	if _, ok := crc.mutation.TownTo(); !ok {
		return &ValidationError{Name: "Town_to", err: errors.New(`ent: missing required field "CarrierRequest.Town_to"`)}
	}
	if _, ok := crc.mutation.AddressFrom(); !ok {
		return &ValidationError{Name: "Address_from", err: errors.New(`ent: missing required field "CarrierRequest.Address_from"`)}
	}
	if _, ok := crc.mutation.AddressTo(); !ok {
		return &ValidationError{Name: "Address_to", err: errors.New(`ent: missing required field "CarrierRequest.Address_to"`)}
	}
	if _, ok := crc.mutation.AutorRequestStatus(); !ok {
		return &ValidationError{Name: "Autor_request_status", err: errors.New(`ent: missing required field "CarrierRequest.Autor_request_status"`)}
	}
	if v, ok := crc.mutation.AutorRequestStatus(); ok {
		if err := carrierrequest.AutorRequestStatusValidator(v); err != nil {
			return &ValidationError{Name: "Autor_request_status", err: fmt.Errorf(`ent: validator failed for field "CarrierRequest.Autor_request_status": %w`, err)}
		}
	}
	if _, ok := crc.mutation.CarrierRequestStatus(); !ok {
		return &ValidationError{Name: "Carrier_request_status", err: errors.New(`ent: missing required field "CarrierRequest.Carrier_request_status"`)}
	}
	if v, ok := crc.mutation.CarrierRequestStatus(); ok {
		if err := carrierrequest.CarrierRequestStatusValidator(v); err != nil {
			return &ValidationError{Name: "Carrier_request_status", err: fmt.Errorf(`ent: validator failed for field "CarrierRequest.Carrier_request_status": %w`, err)}
		}
	}
	return nil
}

func (crc *CarrierRequestCreate) sqlSave(ctx context.Context) (*CarrierRequest, error) {
	if err := crc.check(); err != nil {
		return nil, err
	}
	_node, _spec := crc.createSpec()
	if err := sqlgraph.CreateNode(ctx, crc.driver, _spec); err != nil {
		if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return nil, err
	}
	id := _spec.ID.Value.(int64)
	_node.ID = int(id)
	crc.mutation.id = &_node.ID
	crc.mutation.done = true
	return _node, nil
}

func (crc *CarrierRequestCreate) createSpec() (*CarrierRequest, *sqlgraph.CreateSpec) {
	var (
		_node = &CarrierRequest{config: crc.config}
		_spec = sqlgraph.NewCreateSpec(carrierrequest.Table, sqlgraph.NewFieldSpec(carrierrequest.FieldID, field.TypeInt))
	)
	if value, ok := crc.mutation.RequestID(); ok {
		_spec.SetField(carrierrequest.FieldRequestID, field.TypeInt, value)
		_node.RequestID = value
	}
	if value, ok := crc.mutation.AuthorID(); ok {
		_spec.SetField(carrierrequest.FieldAuthorID, field.TypeInt, value)
		_node.AuthorID = value
	}
	if value, ok := crc.mutation.CarrierID(); ok {
		_spec.SetField(carrierrequest.FieldCarrierID, field.TypeInt, value)
		_node.CarrierID = value
	}
	if value, ok := crc.mutation.Description(); ok {
		_spec.SetField(carrierrequest.FieldDescription, field.TypeString, value)
		_node.Description = value
	}
	if value, ok := crc.mutation.TownFrom(); ok {
		_spec.SetField(carrierrequest.FieldTownFrom, field.TypeString, value)
		_node.TownFrom = value
	}
	if value, ok := crc.mutation.TownTo(); ok {
		_spec.SetField(carrierrequest.FieldTownTo, field.TypeString, value)
		_node.TownTo = value
	}
	if value, ok := crc.mutation.AddressFrom(); ok {
		_spec.SetField(carrierrequest.FieldAddressFrom, field.TypeString, value)
		_node.AddressFrom = value
	}
	if value, ok := crc.mutation.AddressTo(); ok {
		_spec.SetField(carrierrequest.FieldAddressTo, field.TypeString, value)
		_node.AddressTo = value
	}
	if value, ok := crc.mutation.AutorRequestStatus(); ok {
		_spec.SetField(carrierrequest.FieldAutorRequestStatus, field.TypeEnum, value)
		_node.AutorRequestStatus = value
	}
	if value, ok := crc.mutation.CarrierRequestStatus(); ok {
		_spec.SetField(carrierrequest.FieldCarrierRequestStatus, field.TypeEnum, value)
		_node.CarrierRequestStatus = value
	}
	return _node, _spec
}

// CarrierRequestCreateBulk is the builder for creating many CarrierRequest entities in bulk.
type CarrierRequestCreateBulk struct {
	config
	err      error
	builders []*CarrierRequestCreate
}

// Save creates the CarrierRequest entities in the database.
func (crcb *CarrierRequestCreateBulk) Save(ctx context.Context) ([]*CarrierRequest, error) {
	if crcb.err != nil {
		return nil, crcb.err
	}
	specs := make([]*sqlgraph.CreateSpec, len(crcb.builders))
	nodes := make([]*CarrierRequest, len(crcb.builders))
	mutators := make([]Mutator, len(crcb.builders))
	for i := range crcb.builders {
		func(i int, root context.Context) {
			builder := crcb.builders[i]
			builder.defaults()
			var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
				mutation, ok := m.(*CarrierRequestMutation)
				if !ok {
					return nil, fmt.Errorf("unexpected mutation type %T", m)
				}
				if err := builder.check(); err != nil {
					return nil, err
				}
				builder.mutation = mutation
				var err error
				nodes[i], specs[i] = builder.createSpec()
				if i < len(mutators)-1 {
					_, err = mutators[i+1].Mutate(root, crcb.builders[i+1].mutation)
				} else {
					spec := &sqlgraph.BatchCreateSpec{Nodes: specs}
					// Invoke the actual operation on the latest mutation in the chain.
					if err = sqlgraph.BatchCreate(ctx, crcb.driver, spec); err != nil {
						if sqlgraph.IsConstraintError(err) {
							err = &ConstraintError{msg: err.Error(), wrap: err}
						}
					}
				}
				if err != nil {
					return nil, err
				}
				mutation.id = &nodes[i].ID
				if specs[i].ID.Value != nil {
					id := specs[i].ID.Value.(int64)
					nodes[i].ID = int(id)
				}
				mutation.done = true
				return nodes[i], nil
			})
			for i := len(builder.hooks) - 1; i >= 0; i-- {
				mut = builder.hooks[i](mut)
			}
			mutators[i] = mut
		}(i, ctx)
	}
	if len(mutators) > 0 {
		if _, err := mutators[0].Mutate(ctx, crcb.builders[0].mutation); err != nil {
			return nil, err
		}
	}
	return nodes, nil
}

// SaveX is like Save, but panics if an error occurs.
func (crcb *CarrierRequestCreateBulk) SaveX(ctx context.Context) []*CarrierRequest {
	v, err := crcb.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (crcb *CarrierRequestCreateBulk) Exec(ctx context.Context) error {
	_, err := crcb.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (crcb *CarrierRequestCreateBulk) ExecX(ctx context.Context) {
	if err := crcb.Exec(ctx); err != nil {
		panic(err)
	}
}
