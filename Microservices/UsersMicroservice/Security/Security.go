package Security

import (
	"UsersMicroservice/Db"
	"crypto/sha256"
	"encoding/hex"
	"github.com/gofiber/fiber/v2/log"
	"github.com/golang-jwt/jwt/v5"
	"os"
	"time"
)

func GenerateJwtToken(email string) string {
	user, err := Db.GetUserByEmail(email)
	if err != nil {
		log.Error(err)
		return ""
	}

	token, err := Db.AddJwtToDb(user)
	if err != nil {
		log.Error(err)
		return ""
	}

	return token.JWT
}

func GetJwtToken(email string) string {
	user, err := Db.GetUserByEmail(email)
	if err != nil {
		log.Error(err)
		return ""
	}

	token, err := Db.GetJwtFromDb(user)
	if err != nil {
		token, err = Db.AddJwtToDb(user)
		if err != nil {
			log.Error(err)
			return ""
		}
	}

	return token.JWT
}

func ValidatedLogin(email string, password string) bool {
	queriedUser, err := Db.GetUserByEmail(email)

	if err != nil {
		log.Error(err)
		return false
	}

	hash := sha256.New()
	passwordBytes := []byte(password)
	passwordBytes = append(passwordBytes, queriedUser.Salt...)

	hash.Write(passwordBytes)

	return queriedUser.Password == hex.EncodeToString(hash.Sum(nil))
}

func ValidateRegister(email string, password string) bool {
	_, err := Db.CreateUser(email, password)
	if err != nil {
		log.Error(err)
		return false
	}

	return true
}

func ExtractIdFromToken(token string) (int, error) {
	parsedToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) { return []byte(os.Getenv("JWT_SECRET")), nil })
	if err != nil {
		log.Error(err)
		return -1, err
	}

	return int(parsedToken.Claims.(jwt.MapClaims)["userId"].(float64)), nil
}

func TokenValidationHandler(token string) (string, error) {
	parsedToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) { return []byte(os.Getenv("JWT_SECRET")), nil })
	if err != nil {
		log.Error(err)
		return "", &InvalidJWTToken{}
	}

	if parsedToken.Valid {
		return AuthorizeTokenValidationHandler(token)
	}

	log.Error(&InvalidJWTToken{})
	return token, &InvalidJWTToken{}
}

func AuthorizeTokenValidationHandler(token string) (string, error) {
	userId, err := ExtractIdFromToken(token)

	if err != nil {
		return "", err
	}

	user, err := Db.GetUser(userId)
	if err != nil {
		log.Error(err)
		return token, &InvalidJWTToken{}
	}

	dbJwt, err := Db.GetJwtFromDb(user)
	if err != nil {
		log.Error(err)
		return token, &InvalidJWTToken{}
	}

	if token == dbJwt.JWT {
		if dbJwt.ExpiresAt.Before(time.Now()) {
			userNewJwt, err := Db.RenewJwtFromDb(user, dbJwt)
			if err != nil {
				log.Error(err)
				return "", err
			}
			token = userNewJwt.JWT
		}

		log.Info("Successfully Authorized")
		return token, nil
	} else {
		log.Error(&UnauthorizedJWTToken{})
		err := Db.DeleteJwtFromDb(user)
		if err != nil {
			log.Error(err)
		}
		return "", &UnauthorizedJWTToken{}
	}
}
