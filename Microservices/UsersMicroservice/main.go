package main

import (
	"UsersMicroservice/Db"
	"UsersMicroservice/Route"
	fiber "github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/log"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"os"
)

func main() {
	app := fiber.New()

	connectionLog, _ := os.OpenFile("Logs/connection.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	app.Use(logger.New(logger.Config{
		Format: "${time} [${ip}:${port}] ${method} ${status} - ${path}\n",
		Output: connectionLog,
	}))

	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
	}))

	standardLog, _ := os.OpenFile("Logs/standard.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	log.SetOutput(standardLog)

	Route.SetupRoutes(app)

	defer Db.DestroyConnector()

	err := app.Listen(":80")
	if err != nil {
		panic(err)
	}
}
