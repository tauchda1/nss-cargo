package Route

import (
	"UsersMicroservice/Db"
	"UsersMicroservice/Security"
	"errors"
	"github.com/gofiber/fiber/v2"
)

func SetupRoutes(app *fiber.App) {
	_ = app.Post("/login", loginHandler)
	_ = app.Post("/register", registerHandler)
	_ = app.Post("/validate", validateToken)
	_ = app.Post("/change_type", changeType)
}

func loginHandler(ctx *fiber.Ctx) error {
	if Security.ValidatedLogin(ctx.FormValue("user-login-email"), ctx.FormValue("user-login-password")) {
		token := Security.GetJwtToken(ctx.FormValue("user-login-email"))
		if token == "" {
			return ctx.SendStatus(fiber.StatusInternalServerError)
		}

		cookie := new(fiber.Cookie)
		cookie.Name = "user-jwt"
		cookie.Value = token

		ctx.Cookie(cookie)
		return ctx.SendStatus(fiber.StatusNoContent)
	} else {
		return ctx.SendStatus(fiber.StatusUnauthorized)
	}
}

func registerHandler(ctx *fiber.Ctx) error {
	if Security.ValidateRegister(ctx.FormValue("user-register-email"), ctx.FormValue("user-register-password")) {
		token := Security.GenerateJwtToken(ctx.FormValue("user-register-email"))
		if token == "" {
			return ctx.SendStatus(fiber.StatusInternalServerError)
		}
		return ctx.SendStatus(fiber.StatusNoContent)
	} else {
		return ctx.SendStatus(fiber.StatusUnauthorized)
	}
}

func validateToken(ctx *fiber.Ctx) error {
	token := ctx.Cookies("user-jwt", "")
	newToken, err := Security.TokenValidationHandler(token)
	if err != nil {
		if errors.Is(err, &Security.InvalidJWTToken{}) {
			ctx.Status(fiber.StatusUnauthorized)
			return ctx.SendString(err.Error())
		} else if errors.Is(err, &Security.UnauthorizedJWTToken{}) {
			ctx.Status(fiber.StatusForbidden)
			return ctx.SendString(err.Error())
		} else {
			return ctx.SendStatus(fiber.StatusBadRequest)
		}
	}

	cookie := new(fiber.Cookie)
	cookie.Name = "user-jwt"
	cookie.Value = newToken

	ctx.Cookie(cookie)

	return ctx.SendString(newToken)
}

func changeType(ctx *fiber.Ctx) error {
	token := ctx.Cookies("user-jwt", "")
	newToken, err := Security.TokenValidationHandler(token)
	if err != nil {
		if errors.Is(err, &Security.InvalidJWTToken{}) {
			ctx.Status(fiber.StatusUnauthorized)
			return ctx.SendString(err.Error())
		} else if errors.Is(err, &Security.UnauthorizedJWTToken{}) {
			ctx.Status(fiber.StatusForbidden)
			return ctx.SendString(err.Error())
		} else {
			return ctx.SendStatus(fiber.StatusBadRequest)
		}
	}

	newType := ctx.FormValue("user-new-type")
	if newType != "Carrier" && newType != "User" {
		return ctx.SendStatus(fiber.StatusBadRequest)
	}

	userId, err := Security.ExtractIdFromToken(newToken)
	if err != nil {
		return ctx.SendStatus(fiber.StatusInternalServerError)
	}

	user, err := Db.GetUser(userId)
	if err != nil {
		return ctx.SendStatus(fiber.StatusBadRequest)
	}

	_, err = Db.ChangeUserType(user, newType)
	if err != nil {
		return ctx.SendStatus(fiber.StatusBadRequest)
	}

	cookie := new(fiber.Cookie)
	cookie.Name = "user-jwt"
	cookie.Value = newToken

	ctx.Cookie(cookie)

	return ctx.SendStatus(fiber.StatusNoContent)
}
