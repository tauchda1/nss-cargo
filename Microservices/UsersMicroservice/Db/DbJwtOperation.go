package Db

import (
	"UsersMicroservice/Db/ent"
	"context"
	"crypto/rand"
	"github.com/golang-jwt/jwt/v5"
	"os"
	"time"
)

func AddJwtToDb(user *ent.User) (*ent.UserJwt, error) {
	client := GetConnector()

	_, err := GetJwtFromDb(user)
	if err != nil {
		_ = DeleteJwtFromDb(user)
	}

	salt := make([]byte, 20)
	_, err = rand.Read(salt)

	if err != nil {
		return nil, err
	}

	jwtClaims := jwt.MapClaims{
		"userId":   user.ID,
		"userType": user.Type,
		"salt":     salt,
	}

	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, jwtClaims).SignedString([]byte(os.Getenv("JWT_SECRET")))
	if err != nil {
		return nil, err
	}

	now := time.Now()
	future := now.Add(time.Hour * 2)

	userNewJwt, err := client.UserJwt.Create().SetUser(user).SetJWT(token).SetSalt(salt).SetExpiresAt(future).Save(context.Background())
	if err != nil {
		return nil, err
	}

	return userNewJwt, nil
}

func GetJwtFromDb(user *ent.User) (*ent.UserJwt, error) {
	client := GetConnector()
	userJWT, err := client.User.QueryJWT(user).Only(context.Background())
	if err != nil {
		return nil, &ent.NotFoundError{}
	}

	return userJWT, nil
}

func RenewJwtFromDb(user *ent.User, userJWT *ent.UserJwt) (*ent.UserJwt, error) {
	client := GetConnector()

	salt := make([]byte, 20)
	_, err := rand.Read(salt)

	if err != nil {
		return nil, err
	}

	jwtClaims := jwt.MapClaims{
		"userId":   user.ID,
		"userType": user.Type,
		"salt":     salt,
	}

	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, jwtClaims).SignedString([]byte(os.Getenv("JWT_SECRET")))

	now := time.Now()
	future := now.Add(time.Hour * 2)

	userNewJwt, err := client.UserJwt.UpdateOne(userJWT).SetJWT(token).SetSalt(salt).SetExpiresAt(future).Save(context.Background())
	if err != nil {
		panic(err)
	}

	return userNewJwt, nil
}

func DeleteJwtFromDb(user *ent.User) error {
	client := GetConnector()
	userJwt, err := client.User.QueryJWT(user).Only(context.Background())
	if err != nil {
		return &ent.NotFoundError{}
	}
	return client.UserJwt.DeleteOne(userJwt).Exec(context.Background())
}
