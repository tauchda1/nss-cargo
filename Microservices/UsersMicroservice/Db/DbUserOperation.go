package Db

import (
	"UsersMicroservice/Db/ent"
	"UsersMicroservice/Db/ent/user"
	"context"
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"errors"
)

func GetUser(id int) (*ent.User, error) {
	client := GetConnector()
	return client.User.Query().Where(user.ID(id)).Only(context.Background())
}

func GetUserByEmail(email string) (*ent.User, error) {
	client := GetConnector()
	return client.User.Query().Where(user.EmailEQ(email)).Only(context.Background())
}

func CreateUser(email string, unHashedPassword string) (*ent.User, error) {
	client := GetConnector()

	if _, err := GetUserByEmail(email); err == nil {
		return nil, errors.New("user already exists")
	}

	salt := make([]byte, 20)
	_, err := rand.Read(salt)
	if err != nil {
		panic(err)
	}

	hash := sha256.New()
	passwordBytes := []byte(unHashedPassword)
	passwordBytes = append(passwordBytes, salt...)
	hash.Write(passwordBytes)

	password := hash.Sum(nil)
	hexPassword := hex.EncodeToString(password)

	u, err := client.User.
		Create().
		SetEmail(email).
		SetPassword(hexPassword).
		SetSalt(salt).
		Save(context.Background())

	return u, err
}

func ChangeUserType(updatedUser *ent.User, newUserType string) (*ent.User, error) {
	client := GetConnector()

	return client.User.UpdateOne(updatedUser).SetType(user.Type(newUserType)).Save(context.Background())
}
