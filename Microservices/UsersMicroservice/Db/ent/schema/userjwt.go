package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"time"
)

// UserJwt holds the schema definition for the UserJwt entity.
type UserJwt struct {
	ent.Schema
}

// Fields of the UserJwt.
func (UserJwt) Fields() []ent.Field {
	return []ent.Field{
		field.String("JWT").
			NotEmpty(),
		field.Bytes("Salt").
			NotEmpty(),
		field.Time("ExpiresAt").Default(time.Now().Add(time.Duration(1000 * 60 * 60 * 2))),
	}
}

// Edges of the UserJwt.
func (UserJwt) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("User", User.Type).
			Ref("JWT").
			Unique().
			Required(),
	}
}
