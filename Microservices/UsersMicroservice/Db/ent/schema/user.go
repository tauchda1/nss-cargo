package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// User holds the schema definition for the User entity.
type User struct {
	ent.Schema
}

// Fields of the User.
func (User) Fields() []ent.Field {
	return []ent.Field{
		field.String("Email").
			NotEmpty().
			Unique(),
		field.String("Password").
			NotEmpty(),
		field.Enum("Type").
			Values("Admin", "User", "Carrier").
			Default("User"),
		field.Bytes("Salt").
			NotEmpty(),
	}
}

// Edges of the User.
func (User) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("JWT", UserJwt.Type).
			Unique(),
	}
}
