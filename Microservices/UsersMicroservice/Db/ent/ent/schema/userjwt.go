package schema

import "entgo.io/ent"

// UserJwt holds the schema definition for the UserJwt entity.
type UserJwt struct {
	ent.Schema
}

// Fields of the UserJwt.
func (UserJwt) Fields() []ent.Field {
	return nil
}

// Edges of the UserJwt.
func (UserJwt) Edges() []ent.Edge {
	return nil
}
