// Code generated by ent, DO NOT EDIT.

package ent

import (
	"UsersMicroservice/Db/ent/schema"
	"UsersMicroservice/Db/ent/user"
	"UsersMicroservice/Db/ent/userjwt"
	"time"
)

// The init function reads all schema descriptors with runtime code
// (default values, validators, hooks and policies) and stitches it
// to their package variables.
func init() {
	userFields := schema.User{}.Fields()
	_ = userFields
	// userDescEmail is the schema descriptor for Email field.
	userDescEmail := userFields[0].Descriptor()
	// user.EmailValidator is a validator for the "Email" field. It is called by the builders before save.
	user.EmailValidator = userDescEmail.Validators[0].(func(string) error)
	// userDescPassword is the schema descriptor for Password field.
	userDescPassword := userFields[1].Descriptor()
	// user.PasswordValidator is a validator for the "Password" field. It is called by the builders before save.
	user.PasswordValidator = userDescPassword.Validators[0].(func(string) error)
	// userDescSalt is the schema descriptor for Salt field.
	userDescSalt := userFields[3].Descriptor()
	// user.SaltValidator is a validator for the "Salt" field. It is called by the builders before save.
	user.SaltValidator = userDescSalt.Validators[0].(func([]byte) error)
	userjwtFields := schema.UserJwt{}.Fields()
	_ = userjwtFields
	// userjwtDescJWT is the schema descriptor for JWT field.
	userjwtDescJWT := userjwtFields[0].Descriptor()
	// userjwt.JWTValidator is a validator for the "JWT" field. It is called by the builders before save.
	userjwt.JWTValidator = userjwtDescJWT.Validators[0].(func(string) error)
	// userjwtDescSalt is the schema descriptor for Salt field.
	userjwtDescSalt := userjwtFields[1].Descriptor()
	// userjwt.SaltValidator is a validator for the "Salt" field. It is called by the builders before save.
	userjwt.SaltValidator = userjwtDescSalt.Validators[0].(func([]byte) error)
	// userjwtDescExpiresAt is the schema descriptor for ExpiresAt field.
	userjwtDescExpiresAt := userjwtFields[2].Descriptor()
	// userjwt.DefaultExpiresAt holds the default value on creation for the ExpiresAt field.
	userjwt.DefaultExpiresAt = userjwtDescExpiresAt.Default.(time.Time)
}
