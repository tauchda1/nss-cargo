import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import './LoginPage.css';

function LoginPage() {
  const [formData, setFormData] = useState({
    email: '',
    password: ''
  });

  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setError('');
    setLoading(true);

    const urlEncodedData = new URLSearchParams();
    urlEncodedData.append('user-login-email', formData.email);
    urlEncodedData.append('user-login-password', formData.password);

    try {
      const response = await fetch('http://localhost:80/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: urlEncodedData.toString(),
        credentials: 'include',
      });

      setLoading(false);

      if (response.status === 204) {
        navigate('/');
      } else if (response.status === 401) {
        setError('Invalid login credentials.');
      } else {
        setError('An error occurred. Please try again.');
      }
    } catch (error) {
      console.error('Error during fetch:', error);
      setLoading(false);
      setError('Failed to connect to the server. Please try again later.');
    }
  };

  return (
    <div className="login-container">
      <h1>LOGIN</h1>
      <form className="login-form" onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="email">Email</label>
          <input
            type="email"
            id="email"
            name="email"
            value={formData.email}
            onChange={handleChange}
            disabled={loading}
          />
        </div>
        <div className="form-group">
          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            name="password"
            value={formData.password}
            onChange={handleChange}
            disabled={loading}
          />
        </div>
        {error && <p className="error">{error}</p>}
        <button type="submit" className="login-button" disabled={loading}>
          {loading ? 'Logging in...' : 'LOGIN'}
        </button>
      </form>
      <div className="register-link">
        <p>DON'T HAVE AN ACCOUNT? <a href="/register">REGISTER</a></p>
      </div>
    </div>
  );
}

export default LoginPage;