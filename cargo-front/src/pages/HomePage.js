import React from "react";
import "./HomePage.css";

function HomePage() {
  return (
    <div className="home-container">
      <h1>Welcome to the Home Page</h1>
    </div>
  );
}

export default HomePage;
