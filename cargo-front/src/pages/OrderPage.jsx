import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import "./OrderPage.css";

function OrderPage() {
  const [formData, setFormData] = useState({
    description: "",
    townFrom: "",
    townTo: "",
    addressFrom: "",
    addressTo: "",
  });

  const [errors, setErrors] = useState({});
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const validate = () => {
    let formErrors = {};
    if (!formData.description)
      formErrors.description = "Description is required";
    if (!formData.townFrom) formErrors.townFrom = "Town From is required";
    if (!formData.townTo) formErrors.townTo = "Town To is required";
    if (!formData.addressFrom)
      formErrors.addressFrom = "Address From is required";
    if (!formData.addressTo) formErrors.addressTo = "Address To is required";
    return formErrors;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const validationErrors = validate();
    if (Object.keys(validationErrors).length === 0) {
      setErrors({});
      setLoading(true);

      const urlEncodedData = new URLSearchParams();
      urlEncodedData.append("form-send-request-description", formData.description);
      urlEncodedData.append("form-send-request-town-from", formData.townFrom);
      urlEncodedData.append("form-send-request-town-to", formData.townTo);
      urlEncodedData.append("form-send-request-address-from", formData.addressFrom);
      urlEncodedData.append("form-send-request-address-to", formData.addressTo);

      try {
        const response = await fetch("http://localhost:81/request", {
          method: "POST",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
          body: urlEncodedData.toString(),
          credentials: "include",
        });

        setLoading(false);

        if (response.status === 201) {
          navigate("/");
        } else if (response.status === 400) {
          setErrors({ form: "Invalid data. Please check your inputs." });
        } else if (response.status === 401 || response.status === 403) {
          navigate("/login");
        } else {
          setErrors({ form: "An error occurred. Please try again." });
        }
      } catch (error) {
        console.error("Error during fetch:", error);
        setLoading(false);
        setErrors({
          form: "Failed to connect to the server. Please try again later.",
        });
      }
    } else {
      setErrors(validationErrors);
    }
  };

  return (
    <div className="order-container">
      <h1>Create a Transportation Order</h1>
      <form className="order-form" onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="description">Description</label>
          <input
            type="text"
            id="description"
            name="description"
            value={formData.description}
            onChange={handleChange}
            disabled={loading}
          />
          {errors.description && <p className="error">{errors.description}</p>}
        </div>
        <div className="form-group">
          <label htmlFor="townFrom">Town From</label>
          <input
            type="text"
            id="townFrom"
            name="townFrom"
            value={formData.townFrom}
            onChange={handleChange}
            disabled={loading}
          />
          {errors.townFrom && <p className="error">{errors.townFrom}</p>}
        </div>
        <div className="form-group">
          <label htmlFor="townTo">Town To</label>
          <input
            type="text"
            id="townTo"
            name="townTo"
            value={formData.townTo}
            onChange={handleChange}
            disabled={loading}
          />
          {errors.townTo && <p className="error">{errors.townTo}</p>}
        </div>
        <div className="form-group">
          <label htmlFor="addressFrom">Address From</label>
          <input
            type="text"
            id="addressFrom"
            name="addressFrom"
            value={formData.addressFrom}
            onChange={handleChange}
            disabled={loading}
          />
          {errors.addressFrom && <p className="error">{errors.addressFrom}</p>}
        </div>
        <div className="form-group">
          <label htmlFor="addressTo">Address To</label>
          <input
            type="text"
            id="addressTo"
            name="addressTo"
            value={formData.addressTo}
            onChange={handleChange}
            disabled={loading}
          />
          {errors.addressTo && <p className="error">{errors.addressTo}</p>}
        </div>
        {errors.form && <p className="error">{errors.form}</p>}
        <button type="submit" className="order-button" disabled={loading}>
          {loading ? "Submitting..." : "Submit"}
        </button>
      </form>
    </div>
  );
}

export default OrderPage;