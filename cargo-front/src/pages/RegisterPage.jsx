import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import './RegisterPage.css';

function RegisterPage() {
  const [formData, setFormData] = useState({
    email: '',
    password: ''
  });

  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setError('');
    setLoading(true);

    const urlEncodedData = new URLSearchParams();
    urlEncodedData.append('user-register-email', formData.email);
    urlEncodedData.append('user-register-password', formData.password);

    try {
      const response = await fetch('http://localhost:80/register', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: urlEncodedData.toString(),
      });

      setLoading(false);

      if (response.status === 204) {
        navigate('/login');
      } else if (response.status === 401) {
        setError('Email already registered.');
      } else {
        const errorData = await response.json();
        setError(errorData.message || 'An error occurred. Please try again.');
      }
    } catch (error) {
      console.error('Error during fetch:', error);
      setLoading(false);
      setError('Failed to connect to the server. Please try again later.');
    }
  };

  return (
    <div className="register-container">
      <h1>REGISTER</h1>
      <form className="register-form" onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="email">Email</label>
          <input
            type="email"
            id="email"
            name="email"
            value={formData.email}
            onChange={handleChange}
            disabled={loading}
          />
        </div>
        <div className="form-group">
          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            name="password"
            value={formData.password}
            onChange={handleChange}
            disabled={loading}
          />
        </div>
        {error && <p className="error">{error}</p>}
        <button type="submit" className="register-button" disabled={loading}>
          {loading ? 'Registering...' : 'REGISTER'}
        </button>
      </form>

      <div className="register-link">
        <p>
          DO HAVE AN ACCOUNT? <a href="/login">LOGIN</a>
        </p>
      </div>
    </div>
  );
}

export default RegisterPage;
